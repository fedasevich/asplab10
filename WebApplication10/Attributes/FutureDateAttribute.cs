﻿using System.ComponentModel.DataAnnotations;

namespace WebApplication10.Attributes
{
    public class FutureDateAttribute : ValidationAttribute
    {
        public override bool IsValid(object? value)
        {
            if (value is not DateTime dt)
            {
                return false;
            }

            return dt.Date > DateTime.Now.Date;
        }
    }
}
