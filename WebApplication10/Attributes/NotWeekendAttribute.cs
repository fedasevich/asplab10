﻿using System.ComponentModel.DataAnnotations;

namespace WebApplication10.Attributes
{
    public class NotWeekendAttribute : ValidationAttribute
    {
        public override bool IsValid(object? value)
        {
            if (value is not DateTime dt)
            {
                return false;
            }

            return dt.DayOfWeek != DayOfWeek.Saturday && dt.DayOfWeek != DayOfWeek.Sunday;
        }
    }
}
