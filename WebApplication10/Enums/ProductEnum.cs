﻿using System.ComponentModel.DataAnnotations;
using System.Xml.Linq;

namespace WebApplication10.Enums
{
    public enum ProductEnum
    {
        [Display(Name = "JavaScript")]
        JavaScript,

        [Display(Name = "C#")]
        CSharp,

        [Display(Name = "Java")]
        Java,

        [Display(Name = "Python")]
        Python,

        [Display(Name = "Basics")]
        Basics
    }

}
