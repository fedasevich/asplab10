using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using WebApplication10.Enums;
using WebApplication10.Models;

namespace WebApplication10.Pages
{
    [IgnoreAntiforgeryToken]
    public class OrderModel : PageModel
    {
        [BindProperty]
        public Order Order { get;  set; }

        public void OnGet()
        {
        }

        public IActionResult OnPost()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            if (Order.Product == ProductEnum.Basics &&
                Order.Date.DayOfWeek == DayOfWeek.Monday)
            {
                ModelState.AddModelError("Order.Date", "Order for 'Basics' product can't be scheduled on Monday.");
                return Page();
            }

            TempData["SuccessMessage"] = "Form submitted successfully!";

            return Page();
        }
    }

}
