﻿using System.ComponentModel.DataAnnotations;
using WebApplication10.Enums;
using WebApplication10.Attributes;

namespace WebApplication10.Models
{
    public class Order
    {
        [Required(ErrorMessage = "Please enter your full name")]
        [Display(Name = "Full Name")]
        [DataType(DataType.Text)]
        public string FullName { get; set; }

        [Required(ErrorMessage = "Please enter your email")]
        [EmailAddress(ErrorMessage = "Invalid email address")]
        [DataType(DataType.EmailAddress)]
        [UIHint("EmailAddress")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Please select a desired date")]
        [FutureDate(ErrorMessage = "Desired date should be in the future")]
        [NotWeekend(ErrorMessage = "Consultation cannot be on weekends")]
        [Display(Name = "Desired Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime Date { get; set; } = DateTime.Today.AddDays(1);

        [Required(ErrorMessage = "Please select a product for consultation")]
        [Display(Name = "Product")]
        public ProductEnum Product { get; set; }
    }
}